﻿using BulkyBookWeb.Data;
using BulkyBookWeb.Models;
using Microsoft.AspNetCore.Mvc;

namespace BulkyBookWeb.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            IEnumerable<Category> categoryList = _context.Categories.ToList();
            return View(categoryList);
        }

        //GET
        public IActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category category)
        {
            if (category.Name == category.DisplayOrder.ToString())
            {
                //ModelState.AddModelError("CustomError", "The display order cannot exactly match the Name.");
                ModelState.AddModelError("name", "The display order cannot exactly match the Name.");
            }
            if (ModelState.IsValid)
            {
                _context.Categories.Add(category);
                _context.SaveChanges();
                TempData["success"] = "Category created successfully";
                return RedirectToAction("Index", "Category");
            }
            return View(category);
        }

        //GET
        public IActionResult Edit(int? id)
        {
            if(id is null || id <= 0)
                return NotFound();
            //var categoryFirst = _context.Categories.FirstOrDefault(c => c.Id.Equals(id));
            //var categorySingle = _context.Categories.SingleOrDefault(c => c.Id.Equals(id));
            var categoryFind = _context.Categories.Find(id);
            if (categoryFind is null)
                return NotFound();
            return View(categoryFind);
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category category)
        {
            if (category.Name == category.DisplayOrder.ToString())
            {
                //ModelState.AddModelError("CustomError", "The display order cannot exactly match the Name.");
                ModelState.AddModelError("name", "The display order cannot exactly match the Name.");
            }
            if (ModelState.IsValid)
            {
                _context.Categories.Update(category);
                _context.SaveChanges();
                TempData["success"] = "Category updated successfully";
                return RedirectToAction("Index", "Category");
            }
            return View(category);
        }

        //GET
        public IActionResult Delete(int? id)
        {
            if (id is null || id <= 0)
                return NotFound();
            //var categoryFirst = _context.Categories.FirstOrDefault(c => c.Id.Equals(id));
            //var categorySingle = _context.Categories.SingleOrDefault(c => c.Id.Equals(id));
            var categoryFind = _context.Categories.Find(id);
            if (categoryFind is null)
                return NotFound();
            return View(categoryFind);
        }

        //POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var category = _context.Categories.Find(id);
            if (category is null)
                return NotFound();
            _context.Categories.Remove(category);
            _context.SaveChanges();
            TempData["success"] = "Category deleted successfully";
            return RedirectToAction("Index", "Category");
        }
    }
}
